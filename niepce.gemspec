
require_relative 'lib/version'

D = 'Niepce is an alternative open source self-hosted photo-management webapp'.freeze

F = Dir['lib/*.rb'] + Dir['lib/niepce.db3'] + Dir['lib/config5.ru']
F += Dir['bin/niepce'] + Dir['lib/ui5/**/*'] + Dir['lib/ui5_1way/**/*']
F += Dir["lib/openui5-runtime-#{OpenUI5::VERSION}/**/*"]

Gem::Specification.new do |s|
  s.name        = 'niepce'
  s.version     = FiveApples::VERSION
  s.summary     = 'Niepce is an alternative open source self-hosted photo-management webapp'
  s.description = D
  s.authors     = ['oz']
  s.email       = 'dev@aithscel.eu'
  s.files       = F
  s.bindir      = 'bin'
  s.executables << 'niepce'
  s.rdoc_options << "--exclude=lib/openui5-runtime*"
  s.required_ruby_version = '>= 2.4.0'
  s.add_dependency 'safrano', '~> 0.4', '>= 0.4.5'
  s.add_dependency 'thin', '~> 1.7', '>= 1.7.0'
  s.add_dependency 'sqlite3', '~> 1.3', '>= 1.3.9'
  s.add_dependency 'mini_magick', '~> 4.10', '>= 4.10.1'
  s.add_dependency 'exiftool_vendored', '~> 12.0', '>= 12.0.0'
  s.add_development_dependency 'rake', '~> 12.3'
  s.add_development_dependency 'capybara', '~> 3.0'
  s.add_development_dependency 'rubocop', '~> 0.51'
  s.add_development_dependency  'selenium-webdriver', '~> 3.141'
  s.homepage = 'https://gitlab.com/dm0da/niepce'
  s.license = 'MIT'
  s.metadata = {
    "bug_tracker_uri" => "https://gitlab.com/dm0da/niepce/issues",
    "changelog_uri" => "https://gitlab.com/dm0da/niepce/blob/master/CHANGELOG",
    "source_code_uri" => "https://gitlab.com/dm0da/niepce/tree/master",
    "wiki_uri" => "https://gitlab.com/dm0da/niepce/wikis/home"
  }
end
