# frozen_string_literal: true
require 'safrano'
require 'mini_magick'


require 'rack'
require 'fileutils'

module Safrano
  module Media
    class StaticTreeThumbs < StaticTree
      def check_before_create(data: , entity: , filename: )
        if data.is_a? StringIO
          if data.size == 0
            return  Safrano::BadRequestEmptyMediaUpload.new(filename)
          end
        end
        if ( dupl=::Media.first(md5: entity.md5) )   #duplicate
          Safrano::UnprocessableEntityError.new("Duplicate upload detected: #{filename} was already uploaded as media entity with id #{dupl.id}")
        else
          Contract::OK
        end                      
      end
     # minimal working implementation...
      #  Note: @file_server works relative to @root directory
      def odata_get(request:, entity:)
        if ( th = request.params['thumbnail'] )
          odata_get_thumbnail(request: request, entity: entity, thumbnail: th)
        else
          super(request: request, entity: entity)
        end
      end
      
      # relative to @root
      # eg Photo/1/1
      def thumbnail_file(entity: , thumbnail: )
        Dir.chdir(abs_path(entity)) do
          # simple design: one file per directory, and the directory
          # contains the media entity-id --> implicit link between the media
          # entity
          fnvers = Dir.glob('*').max
          thmb = "0t_#{fnvers}_#{thumbnail}"
          File.join(media_path(entity), thmb)
        end
      end
      
      def odata_get_thumbnail(request: , entity: , thumbnail: )
        media_env = request.env.dup
        media_env['PATH_INFO'] = thumbnail_file(entity: entity, 
                                                thumbnail: thumbnail )
        fsret = @file_server.call(media_env)
        if fsret.first == 200
          # provide own content type as we keep it in the media entity
          fsret[1]['Content-Type'] = entity.content_type
        end
        fsret
      end
     # Here as well, MVP implementation
      def save_file(data:, filename:, entity:)
        Dir.chdir(@abs_klass_dir) do
          in_media_directory(entity) do
            filename = '1'
            File.open(filename, 'wb') { |f| IO.copy_stream(data, f) }
            # Thumbnail 128x128
            begin
              # data is actually implemented as a Tempfile
              image = MiniMagick::Image.open(data.path)
              image.resize '1024x1024'
              image.write '0t_1_1024x1024'
              image.thumbnail '128x128'
              image.write '0t_1_128x128'
            rescue MiniMagick::Invalid => e
# TODO report error somewhere              
            end
          end
        end
      end

# offline helper function
      def build_1024_thumb(entity:)
        Dir.chdir(@abs_klass_dir) do
          in_media_directory(entity) do
            
            # Thumbnail 1024x1024
            if  File.exists?('0t_1_1024x1024')
              puts 'already exists'
            else
              begin
                # data is actually implemented as a Tempfile
                image = MiniMagick::Image.open('1')
                image.resize '1024x1024'
                image.write '0t_1_1024x1024'
              rescue MiniMagick::Invalid => e
                puts 'Error', entity.id
                pp e
              end
              puts 'done'
            end
          end
        end
      end
      
    end
  end
end
