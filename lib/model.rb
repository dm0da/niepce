# frozen_string_literal: true


class Media < Sequel::Model(:media)
  many_to_one :import, class: :Import, key: :import_id
  one_to_one :exif, class: :Exif, key: :id
  def get_and_save_exif
    fn = self.class.media_handler.abs_filename(self)
    ee = Exif.new_from_exiftool(self, fn)

    if ee.values.key? :date_time_original_civil
      self.yr = ee.date_time_original_civil&.year
      self.mth = ee.date_time_original_civil&.month
      self.dy = ee.date_time_original_civil&.day
      save
    end

    ee.save
    
  end
  
  #another hook
  def before_create_media_entity(data: , mimetype: )
    self.md5 = Digest::MD5.file data.path
  rescue StandardError => e
# TODO report error somewhere              
  end
  
  # hook to allow calling external tools on the newly written file
  def after_create_media 
    
    get_and_save_exif
    
  end
end

class Exif < Sequel::Model(:exif)
  def self.new_from_exiftool(media, fn)
    e = self.new
    e.from_exiftool(fn)
    e.id = media.id
    e
  end
  def from_exiftool(fn)
    @exiftool_h = Exiftool.new(fn, '-n').to_hash

    if @exiftool_h.key? :date_time_original
      if ( t = @exiftool_h[:date_time_original])
       @exiftool_h[:date_time_original] = DateTime.strptime(t, '%Y:%m:%d %H:%M:%S')
      end
    end
    set_fields(@exiftool_h, columns, missing: :skip)  
  end
end


class Import < Sequel::Model(:import)
  one_to_many :media, class: :Media, key: :import_id
  def before_create
    self.datetime = Time.now.utc.iso8601
  end
end

## tagging
class Tag < Sequel::Model(:tag)
  one_to_many :media_tag, class: :MediaTag, key: :tag_id
end

class MediaTag < Sequel::Model(:tag_media)
  many_to_one :media, class: :Media, key: :media_id
  many_to_one :tag, class: :Tag, key: :tag_id
end

class Media
  one_to_many :media_tag, class: :MediaTag, key: :media_id
end


# complex type for Tag overview
TagWithMedia = Safrano::ComplexType(:tag => Tag, :media => Media )

### SERVER Part #############################
module Niepce
  class ODataServer < Safrano::ServerApp
    publish_service do
  
      title  'Niepce OData API'
      name  'Niepce::ODataServer'
      namespace  'Niepce'

      path_prefix '/odata'
      server_url "http://#{NiepA.host}:#{NiepA.port}"

      enable_batch 
      
      bugfix_create_response
      
      # complex types
      publish_complex_type TagWithMedia
      
      publish_media_model Media do 
        slug :name 
        use Safrano::Media::StaticTreeThumbs, :root => NiepA.media_root
        add_nav_prop_single :exif 
      end  
      
      publish_model Exif 
      
      publish_model  Tag do
        add_nav_prop_collection :media_tag
      end
      
      publish_model MediaTag do
        unrestrict_primary_key
        add_nav_prop_single :media
        add_nav_prop_single :tag
      end
           
      publish_model Import do
        add_nav_prop_collection :media 
      end
      
      function_import('distinct_media_year')
        .return_collection(Media) do
        
        Media.group(:yr)
      end
      
      function_import('distinct_media_month')
        .input(:yr => Integer)
        .return_collection(Media) do | y |
        # warning: y is the concrete parameter/value hash here !
        # therefore we can pass it directly to sequel where()
          Media.where( y ).group(:mth)
      end
 
       
      function_import('distinct_media_day')
        .input(:yr => Integer, :mth => Integer)
        .return_collection(Media) do | ym |
        # warning: yms is the concrete parameter/value hash here !
        # therefore we can pass it directly to sequel where()
          Media.where( ym ).group(:dy)
      end
      
      function_import('distinct_media_tag')
        .return_collection(TagWithMedia) do
        ret=[]
        Tag.each{ |tag| 
          tm = tag.media_tag
          ret << TagWithMedia.new(tag: tag, media: tm.first.media )  unless tm.empty?
        }
        ret 
      end
      
      function_import('unassigned_media_tag')
        .return(Media) do
        Media.where{tagcount <= 0 }.first
      end    
      
      function_import('last_import_medias')
        .auto_query_parameters
        .return_collection(Media) do
          last_import_id = Import.order(Sequel.desc(:datetime)).first.id
          Media.where(:import_id => last_import_id)
      end         
    end
    
  end
end
