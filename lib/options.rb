# frozen_string_literal: true
require 'getoptlong'

OPTS = GetoptLong.new(
        [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
        [ '--use_cdn', '-c', GetoptLong::NO_ARGUMENT ],
        [ '--libdir', '-I', GetoptLong::REQUIRED_ARGUMENT ],
        [ '--test', '-t', GetoptLong::NO_ARGUMENT ]
        )

   def read_opts
      options = {}
      options[:libdir_add] = nil
      options[:use_cdn] = false
      
        OPTS.each do |opt, arg|
          case opt
            when '--help'
              puts <<-EOF
Usage: niepce [OPTIONS] 

-h, --help             Display this help message.
-c, --use_cdn          Use the openui5 CDN instead of local resource files. Default is using local resources
-I, --libdir LIBDIR    Include LIBDIR in the search path for required modules.
              EOF
              exit 0
          when '--use_cdn'
            options[:use_cdn] = true
          when '--test'
            options[:role] = :test 
          when '--libdir'
            options[:libdir_add] = arg
            if arg
              $LOAD_PATH.unshift(arg) unless $LOAD_PATH.include?(arg)
            end
          
        end
      end
      options
    end 

