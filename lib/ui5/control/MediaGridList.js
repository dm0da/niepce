sap.ui.define(
  [
    "sap/f/GridList",
    "sap/f/GridListRenderer",
    "sap/ui/layout/cssgrid/GridResponsiveLayout",
    "sap/ui/layout/cssgrid/GridSettings",
  ],
  function (GridList, GridListRenderer, GridResponsiveLayout, GridSettings) {
    "use strict";
    let MediaGridList = GridList.extend("niepce.controller.MediaGridList", {
      metadata: {
        library: "niepce.controller",
        properties: {},
      },
      renderer: GridListRenderer,
    });

    MediaGridList.prototype.init = function () {
      GridList.prototype.init.apply(this, arguments);

      let oCustomLayout = new GridResponsiveLayout();
      let oLayout = new GridSettings();
      oLayout.setGridTemplateColumns("repeat(auto-fit, minmax(128px, 1fr))");
      oLayout.setGridGap("0.5rem");
      oCustomLayout.setLayout(oLayout);

      this.setCustomLayout(oCustomLayout);
      this.removeAllItems();
      this.setMode("None");
      this.addStyleClass("niepceImagesGL");
    };
    return MediaGridList;
  }
);
