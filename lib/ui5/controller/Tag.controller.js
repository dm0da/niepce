sap.ui.define(
  ["sap/ui/core/mvc/Controller", 
  "niepce/controller/ByTag", 
  "sap/ui/core/dnd/DropInfo",
  "sap/m/MessageToast",
  "sap/ui/model/odata/v2/ODataModel"],
  function (Controller, ByTag, DropInfo, MessageToast, ODataModel) {
    "use strict";
    return Controller.extend("niepce.controller.Tag", {

      
      onInit: function () {
        this.oByTag = new ByTag(this, "tag", "tagGridList", "tagPage");
               
      },

      onPatternMatched: function (oEvent) {
        this.oByTag.onPatternMatched(oEvent);       
      }

    });
  }
);
