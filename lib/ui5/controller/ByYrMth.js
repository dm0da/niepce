sap.ui.define(["niepce/controller/ByDateBase", "sap/m/Title"], function (ByDateBase, Title) {
  "use strict";
  return ByDateBase.extend("niepce.controller.ByYrMth", {
    constructor: function (oParent, route, gridlistid, pageid) {
      ByDateBase.call(this, oParent, route, gridlistid, pageid);
    },
    onPatternMatched: function (oEvent) {
      this.yr = oEvent.getParameter("arguments").id;
      this.mth = oEvent.getParameter("arguments").mm;
      this.oPage.setTitle(this.oBundle.getText("mm" + this.mth) + " " + this.yr);
      this.resetImages();
      this.loadImages();
    },
    imageTitle: function (oEntry) {
      var oTitle = new Title();
      var ody = "";
      if (ody == null) {
        ody = "Date unknown";
      } else {
        ody = oEntry.dy;
      }

      return oTitle.setWidth("100%").setTextAlign("Center").setText(ody);
    },
    imageNav: function (oEvent) {
      var oBindingContext = oEvent.getSource().getBindingContext();
      var oYr = oBindingContext.getProperty("yr");
      if (oYr == null) {
        oYr = "null";
      }
      var oMM = oBindingContext.getProperty("mth");
      if (oMM == null) {
        oMM = "null";
      }
      var ody = oBindingContext.getProperty("dy");
      if (ody == null) {
        ody = "null";
      }
      this.oRouter.navTo("yearMonthDay", { id: oYr, mm: oMM, dd: ody });
    },
    loadImages: function () {
      this.resetImages();
      this.oModel.read("/distinct_media_day", {
        urlParameters: { yr: this.yr, mth: this.mth },
        success: this.readimsuccs.bind(this),
        error: this.readimerr.bind(this),
      });
    },
  });
});
