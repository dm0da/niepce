sap.ui.define(
  [
    "jquery.sap.global",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/odata/ODataModel",
  ],
  function (jQuery, MessageToast, Fragment, Controller, Filter, ODataModel) {
    "use strict";

    var CController = Controller.extend("niepce.controller.NiepceApp", {
      
    });

    return CController;
  }
);
