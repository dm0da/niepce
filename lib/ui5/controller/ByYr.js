sap.ui.define(["niepce/controller/ByDateBase", "sap/m/Title"], function (ByDateBase, Title) {
  "use strict";
  return ByDateBase.extend("niepce.controller.ByYr", {
    constructor: function (oParent, route, gridlistid, pageid) {
      ByDateBase.call(this, oParent, route, gridlistid, pageid);
    },
    onPatternMatched: function (oEvent) {
      this.yr = oEvent.getParameter("arguments").id;
      this.oPage.setTitle(this.yr);
      this.resetImages();
      this.loadImages();
    },
    imageTitle: function (oEntry) {
      var oTitle = new Title();
      var oMM = "";
      if (oMM == null) {
        oMM = "Date unknown";
      } else {
        // i18n month texs in mm1, mm2, mm3 etc.
        oMM = this.oBundle.getText("mm" + oEntry.mth);
      }

      return oTitle.setWidth("100%").setTextAlign("Center").setText(oMM);
    },
    imageNav: function (oEvent) {
      var oBindingContext = oEvent.getSource().getBindingContext();
      var oYr = oBindingContext.getProperty("yr");
      var oMM = oBindingContext.getProperty("mth");

      if (oYr === null && oMM === null) {
        this.oRouter.navTo("yearMonthDay", { id: "null", mm: "null", dd: "null" });
      } else {
        this.oRouter.navTo("yearMonthList", { id: oYr, mm: oMM });
      }
    },
    loadImages: function () {
      this.resetImages();
      this.oModel.read("/distinct_media_month", {
        urlParameters: { yr: this.yr },
        success: this.readimsuccs.bind(this),
        error: this.readimerr.bind(this),
      });
    },
  });
});
