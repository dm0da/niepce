sap.ui.define(["sap/ui/core/mvc/Controller"], function (Controller) {
  "use strict";
  return Controller.extend("niepce.controller.NotFound", {
    onInit: function () {},
  });
});
