sap.ui.define(["niepce/controller/ByDateBase", "sap/m/Title"], function (ByDateBase, Title) {
  "use strict";
  return ByDateBase.extend("niepce.controller.ByDateMain", {
    constructor: function (oParent, route, gridlistid, pageid) {
      ByDateBase.call(this, oParent, route, gridlistid, pageid);
    },
    imageTitle: function (oEntry) {
      var oTitle = new Title();
      var oYr = oEntry.yr;
      if (oYr == null) {
        oYr = "Date unknown";
      }

      return oTitle.setWidth("100%").setTextAlign("Center").setText(oYr);
    },
    imageNav: function (oEvent) {
      var oBindingContext = oEvent.getSource().getBindingContext();
      var oYr = oBindingContext.getProperty("yr");
      if (oYr == null) {
        oYr = "null";
        
        this.oRouter.navTo("yearMonthDay", { id: oYr, mm: 'null', dd: 'null' });
      } else {
        this.oRouter.navTo("yearList", { id: oYr });
      }
    },

    loadImages: function () {
      this.resetImages();
      this.oModel.read("/distinct_media_year", { success: this.readimsuccs.bind(this), error: this.readimerr.bind(this) });
    },
  });
});
