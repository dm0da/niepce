sap.ui.define(
  ["sap/ui/core/UIComponent", "sap/ui/core/mvc/Controller", "sap/m/MessageToast"],
  function (UIComponent, Controller, MessageToast) {
    "use strict";
    return Controller.extend("niepce.controller.CreateTag", {
      onInit: function () {
        this.oRouter = UIComponent.getRouterFor(this);
        this.oRouter.getRoute("createTag").attachPatternMatched(this.createEntry, this);
      },
      createEntry: function () {
        var oView = this.getView();
        var oModel = this.getOwnerComponent().getModel();
        var oContext = oModel.createEntry("/Tag", {});
        oView.setModel(oModel);
        oView.setBindingContext(oContext);
      },

      onNavBack: function () {
        this.oRouter.navTo("tags");
      },
      onErrorHandler: function (error) {
        MessageToast.show("Create tag failed");
      },
      onSuccessHandler: function (data, respo) {
        MessageToast.show("Create success");
        this.oRouter.navTo("tags");
      },
      onCreatePress: function () {
        var oModel = this.getView().getModel();

        var mParms = {};

        mParms.success = this.onSuccessHandler.bind(this);
        mParms.error = this.onErrorHandler.bind(this);

        oModel.submitChanges(mParms);
        //this.oRouter.navTo("tags");
      },
    });
  }
);
