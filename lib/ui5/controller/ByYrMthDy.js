sap.ui.define(
  [
    "niepce/controller/ByDateBase",
    "sap/m/Title",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Sorter",
    "sap/f/GridListItem",
    "sap/ui/model/Context",
    "sap/m/Image",
    "sap/ui/core/dnd/DragInfo",
    "sap/ui/core/dnd/DropInfo",
    "sap/ui/core/dnd/DropPosition",
    "sap/ui/core/dnd/DropLayout",
    "sap/ui/core/dnd/DragDropBase"
  ],
  function (ByDateBase, Title, Filter, FilterOperator, Sorter, GridListItem, Context, Image, 
            DragInfo, DropInfo, DropPosition, DropLayout, DragDropBase) {
    "use strict";
    return ByDateBase.extend("niepce.controller.ByYrMthDy", {
      constructor: function (oParent, route, gridlistid, pageid) {
        ByDateBase.call(this, oParent, route, gridlistid, pageid);
        this.oCarouselVisible = false;
        this.oCarousel = oParent.byId("carousel");
        this.oCarouselCont = oParent.byId("carouselContainer");
        this.oCarouselPages = {};
      },
      hideCarousel: function () {
        this.oCarouselVisible = false;
        this.oCarousel.setVisible(false);
        this.oCarouselCont.setVisible(false);
        this.oImages.setVisible(true);
      },
      showCarousel: function () {
        this.oImages.setVisible(false);

        this.oCarouselVisible = true;
        this.oCarousel.setVisible(true);
        this.oCarouselCont.setVisible(true);
      },
      onNavBack: function () {
        if (this.oCarouselVisible) {
          this.hideCarousel();
        } else {
          var sPreviousHash = this.oHistory.getPreviousHash();

          if (sPreviousHash !== undefined) {
            window.history.go(-1);
          } else {
            this.oRouter.navTo("mainList", true);
          }
        }
      },
      onPatternMatched: function (oEvent) {
        this.yr = oEvent.getParameter("arguments").id;
        this.mth = oEvent.getParameter("arguments").mm;
        this.dy = oEvent.getParameter("arguments").dd;
        this.yrFilter = new Filter({ path: "yr", operator: FilterOperator.EQ, value1: this.yr });
        this.mthFilter = new Filter({ path: "mth", operator: FilterOperator.EQ, value1: this.mth });
        this.dyFilter = new Filter({ path: "dy", operator: FilterOperator.EQ, value1: this.dy });
        this.nameSorter = new Sorter("name", true, true);

        this.oPage.setTitle(this.dy + " " + this.oBundle.getText("mm" + this.mth) + " " + this.yr);

        this.resetCarousel();
        this.resetImages();
        this.loadImages();
        this.hideCarousel();
      },
      resetCarousel: function () {
        this.oCarouselPages = {};
        this.oCarousel.destroyPages();
      },
      imageTitle: function () {
        var oTitle = new Title();
        return oTitle;
        //        return oTitle.setWidth("100%").setTextAlign("Center").setText(oEntry.name);
      },
      imageNav: function (oEvent) {
        var oBindingContext = oEvent.getSource().getBindingContext();
        var oYr = oBindingContext.getProperty("yr");
        if (oYr == null) {
          oYr = "null";
        }
        var oMM = oBindingContext.getProperty("mth");
        if (oMM == null) {
          oMM = "null";
        }
        var ody = oBindingContext.getProperty("dy");
        if (ody == null) {
          ody = "null";
        }
        var oId = oBindingContext.getProperty("id");
        if (oId == null) {
          //  this.oRouter.navTo(...
        } else {
          this.showCarousel();
          //          this.oCarousel.setActivePage(oId);
          this.oCarousel.setActivePage(this.oCarouselPages[oId]);
        }
      },
      handleImageNav(oItem, oImage, oEntry) {
        oItem.setType("Active").attachPress(oEntry, this.imageNav, this);
      },
      loadImages: function () {
        this.resetImages();
        this.oModel.read("/Media", {
          urlParameters: { $expand: "exif" },
          filters: [this.yrFilter, this.mthFilter, this.dyFilter],
          sorters: [this.nameSorter],
          success: this.readimsuccs.bind(this),
          error: this.readimerr.bind(this),
        });
      },
      addImage: function (oEntry) {
        var oItem = new GridListItem();
        var oImage = new Image();
        this.handleImageNav(oItem, oImage, oEntry);

        var oPath = "/Media(" + oEntry.id + ")";
        var oBindingContext = new Context(this.oModel, oPath);
        oItem.setBindingContext(oBindingContext);

        oImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=128x128");

        this.handleItemImageLayout(oItem, oImage);

        this.oImages.addItem(oItem);
        this.oImages.setMode("MultiSelect"); 
        // Carousel
        var oCImage = new Image();

        oCImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=1024x1024");

        oCImage.setDecorative(false);

        this.oCarouselPages[oEntry.id] = oCImage;
        this.oCarousel.addPage(oCImage);
        this.oPage.scrollTo(0);
      },
    });
  }
);
