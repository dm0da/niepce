/**
*
*  https://babeljs.io coalesce
*  in: 
*    a ??= "foo" ;
*  out:
*    "use strict";
*     var _a;
*     (_a = a) !== null && _a !== void 0 ? _a : a = "foo";
* **/


/**  
* https://stackoverflow.com/questions/37376838/how-to-call-utility-functions-in-sapui5-controller 
* 
* **/

sap.ui.define([], function() {
   "use strict";
   
    return {
      coalesce: function (a, defval) {
        return a !== null && a !== void 0 ? a : defval;
    }
   
   };
  }
);
