sap.ui.define(["sap/ui/core/mvc/Controller", "niepce/controller/ByYr"], function (Controller, ByYr) {
  "use strict";
  return Controller.extend("niepce.controller.YearList", {
    onInit: function () {
      this.oByDate = new ByYr(this, "yearList", "yrGridList", "yrPage");
    },
    onPatternMatched: function (oEvent) {
      this.oByDate.onPatternMatched(oEvent);
    },
  });
});
