sap.ui.define(
  [
    "niepce/controller/ByDateBase",
    "sap/m/Title",
    "sap/f/GridListItem",
    "sap/m/Image",
    "sap/m/HBox",
    "sap/m/VBox",
    "sap/ui/model/Context",
  ],
  function (ByDateBase, Title, GridListItem, Image, HBox, VBox, Context) {
    "use strict";
    return ByDateBase.extend("niepce.controller.ByTagMain", {
      constructor: function (oParent, route, gridlistid, pageid) {
        ByDateBase.call(this, oParent, route, gridlistid, pageid);
      },
      imageTitle: function (oTagEntry) {
        var oTitle = new Title();
        var oTg = oTagEntry.name;
        if (oTg == null) {
          oTg = "Tagless";
        }

        return oTitle.setWidth("100%").setTextAlign("Center").setText(oTg);
      },
      imageNav: function (oEvent) {
        var oBindingContext = oEvent.getSource().getBindingContext();
        
        if (oBindingContext == null) {
          this.oRouter.navTo("tagless");
        } else {
          var oTag = oBindingContext.getProperty("id");
          this.oRouter.navTo("tag", { id: oTag });
        }
      },

      loadImages: function () {
        this.resetImages();
        this.oModel.read("/distinct_media_tag", {
          success: this.readimsuccs.bind(this),
          error: this.readimerr.bind(this),
        });
        this.oModel.read("/unassigned_media_tag", {
          success: this.readUnassignedSuccs.bind(this),
          error: this.readimerr.bind(this),
        });
      },
      addImage: function (oEntry) {
        var oItem = new GridListItem();
        var oImage = new Image();
        this.handleImageNav(oItem, oImage, oEntry.tag);

        var oPath = "/Tag(" + oEntry.tag.id + ")";
        var oBindingContext = new Context(this.oModel, oPath);
        oItem.setBindingContext(oBindingContext);

        oImage.setSrc(oEntry.media.__metadata.media_src + "&thumbnail=128x128");

        this.handleItemImageLayout(oItem, oImage, oEntry.tag);

        this.oImages.addItem(oItem);
      },
      addUnassignedImage: function (oEntry) {
        let oItem = new GridListItem();
        let oImage = new Image();
        this.handleImageNav(oItem, oImage, null);

        
        oImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=128x128");
        let dummyTag = {};
        dummyTag.id = null;
        dummyTag.name = 'Unnassigned';
        this.handleItemImageLayout(oItem, oImage, dummyTag);

        this.oImages.addItem(oItem);
      },      
      readUnassignedSuccs: function (oData) {

          this.addUnassignedImage(oData);
        
      },
    });
  }
);
