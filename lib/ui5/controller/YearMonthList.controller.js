sap.ui.define(["sap/ui/core/mvc/Controller", "niepce/controller/ByYrMth"], function (Controller, ByYrMth) {
  "use strict";
  return Controller.extend("niepce.controller.YearMonthList", {
    onInit: function () {
      this.oByDate = new ByYrMth(this, "yearMonthList", "ymGridList", "ymPage");
    },
    onPatternMatched: function (oEvent) {
      this.oByDate.onPatternMatched(oEvent);
    },
  });
});