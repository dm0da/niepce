sap.ui.define(["sap/ui/core/mvc/Controller", "niepce/controller/ByDateMain"], function (Controller, ByDateMain) {
  "use strict";
  return Controller.extend("niepce.controller.MainList", {
    onInit: function () {
      this.oByDate = new ByDateMain(this, "mainList", "ovGridList", "mainPage");
    },
    onPatternMatched: function () {
      this.oByDate.onPatternMatched();
    }
  });
});
