sap.ui.define(["sap/ui/core/mvc/Controller", "niepce/controller/ByTagMain"], 
function (Controller, ByTagMain) {
  "use strict";
  return Controller.extend("niepce.controller.TagList", {
    onInit: function () {
      this.oByTag = new ByTagMain(this, "tags", "ovTagGridList", "tagPage");
    },
    onPatternMatched: function () {
      this.oByTag.onPatternMatched();
    },
    onPressAdd: function() {
        this.oByTag.oRouter.navTo("createTag");
      },
  });
  
});
