sap.ui.define(
  ["sap/ui/core/mvc/Controller", 
  "niepce/controller/ByYrMthDy", 
  "sap/ui/core/dnd/DropInfo",
  "sap/m/MessageToast",
  "sap/ui/model/odata/v2/ODataModel"],
  function (Controller, ByYrMthDy, DropInfo, MessageToast, ODataModel) {
    "use strict";
    return Controller.extend("niepce.controller.YearMonthDay", {
      onTagChange: function(oEvent) {
        MessageToast.show('Changed!');
          
        var t=oEvent.getSource().getSelectedItem();
//        console.log(t); 
//        console.log(t.getBindingContext()); 
        var tp = t.getBindingContext().getPath() ;
        var tm = tp + '/media_tag/$count';
        this.tagPath = tp;
        this.tagCount = tm;

        this.refreshTagCount();
            
      },
      refreshTagCount: function() {
        this.oTileContent.getModel().read(this.tagCount, {success: this.setCount.bind(this) }) ;
        
      },
      setCount: function(oData){
        this.oTileContent.setValue(oData);
      },
      
      onInit: function () {
        this.oByDate = new ByYrMthDy(this, "yearMonthDay", "ymdGridList", "ymdPage");
        this.oTileContent = this.byId('tileContent');
        this.oTags = this.byId('tagList');
        this.oTags.attachChange(this.onTagChange.bind(this));
        
      },

      onPatternMatched: function (oEvent) {
        this.oByDate.onPatternMatched(oEvent);       
      },
      
      assignMediaTag: function(media){
        // /Tag(26)
        console.log(this.tagPath);
        // /Media(1664)
        var mediaPath = media.getBindingContextPath();
        console.log(mediaPath );
        
        // hackamegaton
        
        var m = this.tagPath.match(/\/Tag\((\d+)\)/);
        var tag_id = m[1];
        m = mediaPath.match(/\/Media\((\d+)\)/);
        var media_id = m[1];
        // MediaTag(tag_id=26,media_id=1664)
        var tp = this.tagPath.replace(')',',media_id=').replace('/','');
        var mt = mediaPath.replace('(', tp.replace('(','(tag_id='));
        console.log(mt);
        var oData = { tag_id: parseInt(tag_id), media_id: parseInt(media_id) };
        console.log(oData);
        this.oModel = this.getView().getModel();
        this.oModel.create('/MediaTag', oData, {
          success: function() { /* do something */ },
          error: function(oError) { /* do something */ }
        });
      },
      
      onDrop: function (oInfo) {
        
        var tags = this.byId('tagList');
        var tags_t = tags.getValue();
        MessageToast.show( tags_t );
        var oDragged = oInfo.getParameter("draggedControl"),

				oGrid = oDragged.getParent(),
				
				aItems = oGrid.getSelectedItems();
        
        for(let i of aItems){
          this.assignMediaTag(i) ;
        }
        
			  this.refreshTagCount();
        return;
      },
    });
  }
);
