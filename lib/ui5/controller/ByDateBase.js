sap.ui.define(
  [
    "sap/ui/base/Object",
    "sap/ui/core/routing/History",
    "sap/ui/core/UIComponent",
    "sap/m/Title",
    "sap/f/GridListItem",
    "sap/m/Image",
    "sap/m/HBox",
    "sap/m/VBox",
    "sap/ui/model/Context",
    "sap/ui/model/resource/ResourceModel"
  ],
  function (Object, History, UIComponent, Title, GridListItem, Image, HBox, VBox, Context, ResourceModel) {
    "use strict";
    return Object.extend("niepce.controller.ByDateBase", {
      constructor: function (oParent, route, gridlistid, pageid) {
        this.parent = oParent;
        this.oRouter = UIComponent.getRouterFor(oParent);
        this.oModel = oParent.getOwnerComponent().getModel();
        this.oRouter.getRoute(route).attachPatternMatched(oParent.onPatternMatched, oParent);

        // Page back nav
        this.oPage = oParent.byId(pageid);
        this.oHistory = History.getInstance();
        this.oPage.attachNavButtonPress({}, this.onNavBack, this);

        // set i18n model on view
        var i18nModel = new ResourceModel({
          bundleName: "niepce.i18n.i18n",
          supportedLocales: [""],
          fallbackLocale: "",
        });
        // read msg from i18n model
        this.oBundle = i18nModel.getResourceBundle();

        this.oImages = oParent.byId(gridlistid);

        this.resetImages();
      },
      onPatternMatched: function () {
        this.resetImages();
        this.loadImages();
      },
      resetImages: function () {
        this.oImages.removeAllItems();
        this.oImages.setMode("None");
        
      },
      onNavBack: function () {
        var sPreviousHash = this.oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          this.oRouter.navTo("mainList", true);
        }
      },
      readimsuccs: function (oData) {
        var i, len;
        var oList = oData["results"];
        for (i = 0, len = oList.length; i < len; ++i) {
          this.addImage(oList[i]);
        }
      },
      
      readimerr: function () {},
      
      handleImageNav(oItem, oImage, oEntry) {
        oItem.setType("Active").attachPress(oEntry, this.imageNav, this);
      },
      
      handleItemImageLayout: function (oItem, oImage, oEntry) {
        oImage.setDecorative(false);


        oItem.addStyleClass("niepceImagesGLI");

        var vBox = new VBox();
        var hBox = new HBox();
  
        vBox.setAlignItems("Center").setWidth("100%");
        vBox.addItem(this.imageTitle(oEntry)).addItem(oImage);
        hBox.setAlignItems("Center").setHeight("100%");

        hBox.addItem(vBox);
        oItem.addContent(hBox);
      },
      addImage: function (oEntry) {
        var oItem = new GridListItem();
        var oImage = new Image();
        this.handleImageNav(oItem, oImage, oEntry);

        var oPath = "/Media(" + oEntry.id + ")";
        var oBindingContext = new Context(this.oModel, oPath);
        oItem.setBindingContext(oBindingContext);

        oImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=128x128");

        this.handleItemImageLayout(oItem, oImage, oEntry);

        this.oImages.addItem(oItem);
      },
    });
  }
);
