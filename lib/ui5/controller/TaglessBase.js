sap.ui.define(
  [
    "niepce/controller/ByDateBase",
    "sap/m/Title",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Sorter",
    "sap/f/GridListItem",
    "sap/ui/model/Context",
    "sap/m/Image"
  ],
  function (ByDateBase, Title, Filter, FilterOperator, Sorter, GridListItem, Context, Image) {
    "use strict";
    return ByDateBase.extend("niepce.controller.TaglessBase", {
      constructor: function (oParent, route, gridlistid, pageid) {
        ByDateBase.call(this, oParent, route, gridlistid, pageid);
        this.oCarouselVisible = false;
        this.oCarousel = oParent.byId("carousel");
        this.oCarouselCont = oParent.byId("carouselContainer");
        this.oCarouselPages = {};
      },
      hideCarousel: function () {
        this.oCarouselVisible = false;
        this.oCarousel.setVisible(false);
        this.oCarouselCont.setVisible(false);
        this.oImages.setVisible(true);
      },
      showCarousel: function () {
        this.oImages.setVisible(false);

        this.oCarouselVisible = true;
        this.oCarousel.setVisible(true);
        this.oCarouselCont.setVisible(true);
      },
      onNavBack: function () {
        if (this.oCarouselVisible) {
          this.hideCarousel();
        } else {
          var sPreviousHash = this.oHistory.getPreviousHash();

          if (sPreviousHash !== undefined) {
            window.history.go(-1);
          } else {
            this.oRouter.navTo("mainList", true);
          }
        }
      },
      setPageTitle: function(){
        this.oPage.setTitle("Tagless media" ); 
      },
      onPatternMatched: function () {
        
        this.tag_path = "/Media"
        
        this.taglessFilter = new Filter({ path: "tagcount", operator: FilterOperator.EQ, value1: '0' });
        this.nameSorter = new Sorter("id", true, true);
      

        this.resetCarousel();
        this.resetImages();
        this.loadImages();
        this.hideCarousel();
      },
      resetCarousel: function () {
        this.oCarouselPages = {};
        this.oCarousel.destroyPages();
      },
      imageTitle: function () {
        var oTitle = new Title();
        return oTitle;
      },
      imageNav: function (oEvent) {
        var oBindingContext = oEvent.getSource().getBindingContext();

        var oId = oBindingContext.getProperty("id");
        if (oId == null) {
          //  this.oRouter.navTo(...
        } else {
          this.showCarousel();
          
          this.oCarousel.setActivePage(this.oCarouselPages[oId]);
        }
      },
      handleImageNav(oItem, oImage, oEntry) {
        oItem.setType("Active").attachPress(oEntry, this.imageNav, this);
      },
      loadImages: function () {
        this.resetImages();
        this.read_skip = 0;
        this.read_top = 30;
        this.loadNextImages();
      },
      loadNextImages: function () {
        this.oModel.read(this.tag_path , {
          urlParameters: { $expand: "exif", $skip: this.read_skip, $top: this.read_top },
          filters: [this.taglessFilter],
          sorters: [this.nameSorter],
          success: this.readimsuccs.bind(this),
          error: this.readimerr.bind(this),
        });
        },
      readimsuccs: function (oData) {
        var i, len;
        this.setPageTitle(oData);
        var oList = oData["results"];
        for (i = 0, len = oList.length; i < len; ++i) {
          this.addImage(oList[i]);
        }
        // get next page of images
        this.read_skip = this.read_skip + this.read_top;
        this.loadNextImages();
      },
      addImage: function (oEntry) {
        
        var oItem = new GridListItem();
        var oImage = new Image();
        this.handleImageNav(oItem, oImage, oEntry);

        var oPath = "/Media(" + oEntry.id + ")";
        var oBindingContext = new Context(this.oModel, oPath);
        oItem.setBindingContext(oBindingContext);

        oImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=128x128");

        this.handleItemImageLayout(oItem, oImage);

        this.oImages.addItem(oItem);

        // Carousel
        var oCImage = new Image();

        oCImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=1024x1024");

        oCImage.setDecorative(false);

        this.oCarouselPages[oEntry.id] = oCImage;
        this.oCarousel.addPage(oCImage);
        this.oPage.scrollTo(0);
      },
    });
  }
);
