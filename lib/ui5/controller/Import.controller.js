sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/core/IconPool",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/model/json/JSONModel",
    "sap/m/UploadCollectionParameter",
    "sap/ui/model/resource/ResourceModel",
    "./Base64",
    "sap/m/MessageItem",
    "sap/m/MessageView",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/m/Bar",
    "sap/m/Text",
  ],
  function (
    Controller,
    MessageToast,
    IconPool,
    ODataModel,
    JSONModel,
    UploadCollectionParameter,
    ResourceModel,
    Base64,
    MessageItem,
    MessageView,
    Button,
    Dialog,
    Bar,
    Text
  ) {
    "use strict";
    return Controller.extend("niepce.controller.Import", {
      onInit: function () {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter.getRoute("importPage").attachPatternMatched(this.onPatternMatched, this);

        this._oImages = this.byId("imGridList");
        this._oImages.removeAllItems();
        this._oImages.setMode("None");

        this._oModel = this.getOwnerComponent().getModel();

        var oText = "Selection for upload:(0)";
        // oUploadCollection.setNumberOfAttachmentsText(oText) ;
        this.byId("uploadTitle").setText(oText);
        //var oToolbar = oUploadCollection.getToolbar();
        //var oButton = new sap.m.Button();
        //oButton.setText("Upload Now");
        //oButton.attachPress(this.onStartUpload);
        //oToolbar.addContent(oButton);
        this._oMessages = [];
        this._uploadState = "Success";
        this._oUploadCollection = this.byId("UploadCollection");
      },
      onPatternMatched: function () {
        this._oModel = this.getOwnerComponent().getModel();
        this._oImages = this.byId("imGridList");
        this._oImages.removeAllItems();
        this._oImages.setMode("None");
        var oIndex = 0;
        this._oMessages = [];
        this._oIndex = oIndex;
      },
      finalizeMessages: function () {
        if (this._oMessages.length == 0) {
          var oMessage = {
            type: "Success",
            title: "Upload Successfull",
            subtitle: "All selected data was successfully imported",
            description: "All selected data was successfully imported",
          };
          this._oMessages.push(oMessage);
          this._uploadState = "Success";
        } else {
          if (this._oMessages.find((e) => e.type == "Error")) {
            this._uploadState = "Error";
          } else if (this._oMessages.find((e) => e.type == "Warning")) {
            this._uploadState = "Warning";
          }
        }
      },
      addMessage: function (oJMess) {
        var type = "Error";
        if (oJMess.code == "422") {
          type = "Warning";
        }
        var oMessage = {
          type: type,
          title: oJMess.type,
          subtitle: oJMess.message,
        };
        if (oJMess.type == "Safrano::BadRequestEmptyMediaUpload") {
          oMessage.title = "Empty upload";
        }
        if (oJMess.type == "Safrano::UnprocessableEntityError") {
          oMessage.title = "Duplicated upload";
        }

        this._oMessages.push(oMessage);
      },
      showMessages: function () {
        var oMessageTemplate = new MessageItem({
          type: "{type}",
          title: "{title}",
          subtitle: "{subtitle}",
        });

        var oModel = new JSONModel();
        oModel.setData(this._oMessages);
        this.oMessageView = new MessageView({
          showDetailsPageHeader: true,
          itemSelect: function () {},
          items: {
            path: "/",
            template: oMessageTemplate,
          },
        });

        this.oMessageView.setModel(oModel);

        this.oDialog = new Dialog({
          resizable: true,
          content: this.oMessageView,
          state: this._uploadState,
          beginButton: new Button({
            press: function () {
              this.getParent().close();
            },
            text: "Close",
          }),
          customHeader: new Bar({
            titleAlignment: sap.m.TitleAlignment.Auto,
            contentMiddle: [new Text({ text: this._uploadState })],
            contentLeft: [],
          }),
          contentHeight: "50%",
          contentWidth: "80%",
          verticalScrolling: false,
        });

        this.oMessageView.navigateBack();
        this.oDialog.open();
      },
      loadImages: function () {
        this._oImages.removeAllItems();

        function _loadimerr() {}

        this._oModel.read("/Media", {
          success: this.loadimsuccs.bind(this),
          error: _loadimerr,
        });
      },
      addImage: function (oEntry) {
        var oItem = new sap.f.GridListItem();
        var oImage = new sap.m.Image();
        oImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=128x128");

        oImage.setDecorative(false);

        oItem.addContent(oImage);
        this._oImages.addItem(oItem);
      },

      loadimsuccs: function (oData) {
        var i, len;
        var oList = oData["results"];
        for (i = 0, len = oList.length; i < len; ++i) {
          this.addImage(oList[i]);
        }
      },

      onChange: function (oEvent) {
        var oUploadCollection = oEvent.getSource();
        // Header Token
        var oCustomerHeaderToken = new UploadCollectionParameter({
          name: "x-csrf-token",
          value: "Fetch",
        });
        oUploadCollection.addHeaderParameter(oCustomerHeaderToken);

        var oCount = oUploadCollection.getItems().length; // current records
        oCount = oCount + oEvent.getParameter("files").length; // added

        var oText = "Selection for upload:(" + oCount + ")";
        //        oUploadCollection.setNumberOfAttachmentsText(oText) ;
        this.byId("uploadTitle").setText(oText);
      },

      onFileDeleted: function () {
        MessageToast.show("fileDeleted ");
      },

      onFilenameLengthExceed: function () {
        MessageToast.show("FilenameLengthExceed error");
      },

      onFileSizeExceed: function () {
        MessageToast.show("FileSizeExceed error");
      },

      onTypeMissmatch: function () {
        MessageToast.show("TypeMissmatch error");
      },
      onImportStartError: function () {
        MessageToast.show("Import start failed");
      },
      onImportStartSuccess: function (data) {
        MessageToast.show("Import start success");

        var import_id = data.results[0].id;
        var uploadUrl = "/Import(" + import_id + ")/media";
        // warning: this only works with intantUpload=true

        // oUploadCollection.setUploadUrl(uploadUrl);
        // HAck !see https://github.com/SAP/openui5/issues/786
        for (var i = 0; i < this._oUploadCollection._aFileUploadersForPendingUpload.length; i++) {
          this._oUploadCollection._aFileUploadersForPendingUpload[i].setUploadUrl(uploadUrl);
        }
        this.doUpload();
      },
      onStartUpload: function () {
        var oUploadNowButton = this.byId("uploadNowButton");

        var cFiles = this._oUploadCollection.getItems().length;

        if (cFiles > 0) {
          oUploadNowButton.setEnabled(false);
          var oModel = this.getOwnerComponent().getModel();
          var oImportEntry = { id: null, datetime: "" };

          oModel.create("/Import", oImportEntry, {
            success: this.onImportStartSuccess.bind(this),
            error: this.onImportStartError.bind(this),
          });
        }
      },

      doUpload: function () {
        this._oImages.removeAllItems();
        this._oIndex = 0;
        this._oFinished = 0;
        this._oMessages = [];
        this._oExpected = this._oUploadCollection.getItems().length;
        MessageToast.show("Upload is called (" + this._oExpected + " file(s) )");
        this._oUploadCollection.upload();
      },

      onBeforeUploadStarts: function (oEvent) {
        // Header Slug

        // according to AtomPub spec, the SLUG has to be RFC2047-encoded
        // we use simply the B-Variant
        var oSlugRaw = oEvent.getParameter("fileName");
        var oSlugB2047 = `=?utf-8?b?${Base64.encode(oSlugRaw)}?=`;

        var oCustomerHeaderSlug = new UploadCollectionParameter({
          name: "SLUG",
          value: oSlugB2047,
        });

        this._oIndex = this._oIndex + 1;
        var oPars = oEvent.getParameters();
        oPars.addHeaderParameter(oCustomerHeaderSlug);
        var oCustomerHeaderIndex = new UploadCollectionParameter({
          name: "Niepce-Upload-Index",
          value: this._oIndex,
        });

        oPars.addHeaderParameter(oCustomerHeaderIndex);
      },

      onUploadComplete: function (oEvent) {
        var oPars = oEvent.getParameter("mParameters"); // mmmm
        var hidx = oPars.requestHeaders.find((h) => h.name === "Niepce-Upload-Index");

        this._oUploadCollection.removeItem(parseInt(hidx.value));
        var oCount = this._oUploadCollection.getItems().length; // current records

        var oText = "Remaining to upload:(" + oCount + ")";
        // oUploadCollection.setNumberOfAttachmentsText(oText) ;
        this.byId("uploadTitle").setText(oText);
        this._oFinished = this._oFinished + 1;

        // finished ?
        if (this._oExpected == this._oFinished) {
          this.finalizeMessages();
          this.showMessages();
          this.reset();
        }

        // feedback
        var raw = oPars.responseRaw;
        var oJSONResp = JSON.parse(raw);
        if (oPars.status == 201) {
          var oEntry = oJSONResp.d.results[0];
          this.addImage(oEntry);
        } else {
          this.addMessage(oJSONResp["odata.error"]);
        }
      },
      reset: function () {
        var oUploadNowButton = this.byId("uploadNowButton");
        oUploadNowButton.setEnabled(true);
        this._oIndex = 0;
        this._oFinished = 0;
        this._oExpected = 0;
        this._oMessages = [];
        this._oUploadCollection.destroyItems();
        this._oUploadCollection._aDeletedItemForPendingUpload = [];
      },
      uploadTerminated: function () {
        // TODO
        this._oFinished = this._oFinished + 1;
        if (this._oExpected == this._oFinished) {
          this.finalizeMessages();
          this.showMessages();
          this.reset();
        }
      },
      onSelectChange: function (oEvent) {
        this._oUploadCollection.setShowSeparators(oEvent.getParameters().selectedItem.getProperty("key"));
      },
    });
  }
);
