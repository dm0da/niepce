sap.ui.define(
  [
    "sap/ui/base/Object",
    "niepce/control/MediaGridList",
    "sap/ui/core/routing/History",
    "sap/ui/core/UIComponent",
    "sap/m/Title",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Sorter",
    "sap/f/GridListItem",
    "sap/m/Image",
    "sap/m/HBox",
    "sap/m/VBox",
    "sap/ui/model/Context",
    "sap/ui/model/resource/ResourceModel",
  ],
  function (
    Object,
    MediaGridList,
    History,
    UIComponent,
    Title,
    Filter,
    FilterOperator,
    Sorter,
    GridListItem,
    Image,
    HBox,
    VBox,
    Context,
    ResourceModel
  ) {
    "use strict";
    return Object.extend("niepce.controller.medialist.Base", {
      constructor: function (oParent, route, gridlistid, pageid, gridcarouid) {
        this.parent = oParent;
        this.gridlistid = gridlistid;
        this.oRouter = UIComponent.getRouterFor(oParent);
        this.oModel = oParent.getOwnerComponent().getModel();
        this.oRouter.getRoute(route).attachPatternMatched(oParent.onPatternMatched, oParent);

        // Page back nav
        this.oPage = oParent.byId(pageid);
        this.oHistory = History.getInstance();
        this.oPage.attachNavButtonPress({}, this.onNavBack, this);

        // set i18n model on view
        var i18nModel = new ResourceModel({
          bundleName: "niepce.i18n.i18n",
          supportedLocales: [""],
          fallbackLocale: "",
        });
        // read msg from i18n model
        this.oBundle = i18nModel.getResourceBundle();

        // Carousel with MediaGridList pages
        this.oGridCarou = oParent.byId(gridcarouid);
        this.oGridCarou.attachBeforePageChanged(this.onBeforeSubPageChanged.bind(this));
      },

      // Abstract Methods
      entryTitle() {
        throw new Error("Implementation of abstract method entryTitle(oEntry) missing");
      },
      setPageTitle() {
        throw new Error("Implementation of abstract method setPageTitle missing");
      },
      imageNav() {
        throw new Error("Implementation of abstract method imageNav(oEvent) missing");
      },
      setItemBindingForNav() {
        throw new Error("Implementation of abstract method setItemBindingForNav(oItem, oEntry) missing");
      },
      setImageThumbSrc() {
        throw new Error("Implementation of abstract method setImageThumbSrc(oImage, oEntry) missing");
      },
      setReadParameters() {
        throw new Error("Implementation of abstract method setReadParameters missing");
      },
      getEntries() {
        throw new Error("Implementation of abstract method getEntries(oData) missing");
      },
      // End of Abstract Section

      onBeforeSubPageChanged(oEvent) {
        let aPages = oEvent.getParameter("activePages");
        this.iSubPage = aPages[0];
        if ( this.iSubPage == 0 ) { return } ;
           
        this.oImages = this.aSubPages[this.iSubPage];
        let oList = this.aSubPagesData[this.iSubPage];
        if (this.oImages.getItems().length != oList.length) {
          this.oImages.removeAllItems();
          this.addImagesToSubPage(oList);
        }
      },
      onPatternMatched: function () {
        this.resetSubPages();
        this.loadSubPages();
      },
      resetImages: function () {
        this.oImages.removeAllItems();
        this.oImages.setMode("None");
      },
      resetSubPages() {
        this.oGridCarou.destroyPages();
        this.oGridCarou.removeAllPages();
        this.aSubPages = [];
        this.aSubPagesData = [];
        this.iSubPage = 0;
        this.iLastSubPage = 0;
        this.iRead = 0 ;
      },
      imageTitle: function (oEntry) {
        let oTitle = new Title();
        let otxt = this.entryTitle(oEntry);

        return oTitle.setWidth("100%").setTextAlign("Center").setText(otxt);
      },
      handleImageNav: function (oItem, oEntry) {
        oItem.setType("Active").attachPress(oEntry, this.imageNav, this);
      },
      handleItemImageLayout: function (oItem, oImage, oEntry) {
        oImage.setDecorative(false);

        oItem.addStyleClass("niepceImagesGLI");

        var vBox = new VBox();
        var hBox = new HBox();

        vBox.setAlignItems("Center").setWidth("100%");
        vBox.addItem(this.imageTitle(oEntry)).addItem(oImage);
        hBox.setAlignItems("Center").setHeight("100%");

        hBox.addItem(vBox);
        oItem.addContent(hBox);
      },
      addImage: function (oEntry) {
        var oItem = new GridListItem();
        var oImage = new Image();
        this.handleImageNav(oItem, oEntry);
        this.setItemBindingForNav(oItem, oEntry);

        this.setImageThumbSrc(oImage, oEntry);

        this.handleItemImageLayout(oItem, oImage, oEntry);

        this.oImages.addItem(oItem);
      },
      loadSubPages() {
        this.resetSubPages();
        this.setReadParameters();
        // create first subPage that will receive the first set of images read
        this.addSubPage();
        this.oGridCarou.setVisible(true);
        this.oGridCarou.setActivePage(0);
        // read first batch of images
        this.loadFirstPage();
      },

      addSubPage() {
        // build and add media images grid list to page content
        let lmediaGridListSubId = this.gridlistid + "_" + this.iLastSubPage;
        let gmediaGridListSubId = this.parent.createId(lmediaGridListSubId)

        let oImages = new MediaGridList(gmediaGridListSubId);
        
        this.aSubPages[this.iLastSubPage] = oImages;
        this.oGridCarou.addPage(oImages);
      },
      loadFirstPage() {
        this.oModel.read(this.read_path, {
          urlParameters: { $expand: this.read_expand, 
                           $inlinecount: 'allpages',
                           $skip: this.read_skip, $top: this.read_top },
          filters: this.read_filters,
          sorters: this.read_filter,
          success: this.readFirstPageSuccs.bind(this),
          error: this.readimerr.bind(this),
        });
      },
      loadNextPages() {
        this.oModel.read(this.read_path, {
          urlParameters: { $expand: this.read_expand, $skip: this.read_skip, $top: this.read_top },
          filters: this.read_filters,
          sorters: this.read_filter,
          success: this.readNextPagesSuccs.bind(this),
          error: this.readimerr.bind(this),
        });
      },
      saveSubPageData(aList) {
        this.aSubPagesData[this.iRead] = aList;
      },
      addImagesToSubPage: function (aList) {
        for (let oEntry of aList) {
          this.addImage(oEntry);
        }
      },
      createAllSubPages(){
        if ( this.iCount > this.read_top){
          let iSubPageCount = Math.trunc(this.iCount / this.read_top)  ;
          for(let i=1; i < iSubPageCount; i++){
            this.iLastSubPage = this.iLastSubPage + 1;
            this.addSubPage();
          }
        }
      },
      readFirstPageSuccs: function (oData) {
        this.setPageTitle(oData);
        let aList = this.getEntries(oData);
        this.iCount = this.getEntriesCount(oData);        
        this.iSubPage = 0;
        this.iLastSubPage = 0;
        this.iRead = 0;
        this.oImages = this.aSubPages[this.iLastSubPage];
        this.addImagesToSubPage(aList);
        // create all needed blank subPages placeholders
        this.createAllSubPages();
        
        if (aList.length > 0) {
          // get next page of images
          this.read_skip = this.read_skip + this.read_top;
          this.iRead = this.iRead + 1 ;          
          this.loadNextPages();
        }
      },
      readNextPagesSuccs: function (oData) {
        let aList = this.getEntries(oData);

        // dont add the read Images to the subPage list but save them for later
        this.saveSubPageData(aList);
        if (aList.length > 0) {
          // get next page of images
          this.read_skip = this.read_skip + this.read_top;
          this.iRead = this.iRead + 1 ;
          this.loadNextPages();
        }
      },

      readimerr: function () {},
      onNavBack: function () {
        var sPreviousHash = this.oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          this.oRouter.navTo("mainList", true);
        }
      },
    });
  }
);
