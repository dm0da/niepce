sap.ui.define(
  ["niepce/controller/medialist/Base", "sap/ui/model/Context", "niepce/controller/Utils"],
  function (MediaListBase, Context, Utils) {
    "use strict";
    return MediaListBase.extend("niepce.controller.medialist.Search", {
      constructor: function (oParent, route, gridlistid, pageid, gridcarouid) {
        MediaListBase.call(this, oParent, route, gridlistid, pageid, gridcarouid);
      },

      setPageTitle() {
        this.oPage.setTitle("Media search page");
      },
      entryTitle(oEntry) {
        return Utils.coalesce(oEntry.name, "Unknown");
      },
      setItemBindingForNav: function (oItem, oEntry) {
        let oPath = "/Media(" + oEntry.id + ")";
        let oBindingContext = new Context(this.oModel, oPath);
        oItem.setBindingContext(oBindingContext);
      },
      imageNav: function (oEvent) {
        var oBindingContext = oEvent.getSource().getBindingContext();
        var oYr = oBindingContext.getProperty("yr");
        if (oYr == null) {
          oYr = "null";

          this.oRouter.navTo("yearMonthDay", { id: oYr, mm: "null", dd: "null" });
        } else {
          this.oRouter.navTo("yearList", { id: oYr });
        }
      },
      setReadParameters() {
        this.read_skip = 0;
        this.read_top = 100;
        this.read_path = "/Media";
        this.read_expand = "exif";
      },
      getEntriesCount(oData) {
        return oData["__count"] ;
      },
      getEntries(oData) {
        return oData["results"] ;
      },
      setImageThumbSrc: function (oImage, oEntry) {
        oImage.setSrc(oEntry.__metadata.media_src + "&thumbnail=128x128");
      },
    });
  }
);
