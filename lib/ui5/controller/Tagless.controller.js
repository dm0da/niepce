sap.ui.define(
  ["sap/ui/core/mvc/Controller", 
  "niepce/controller/TaglessBase"],
  function (Controller, TaglessBase) {
    "use strict";
    return Controller.extend("niepce.controller.Tagless", {

      
      onInit: function () {
        this.oTagless = new TaglessBase(this, "tagless", "taglessGridList", "taglessPage");
      },

      onPatternMatched: function (oEvent) {
        this.oTagless.onPatternMatched(oEvent);       
      }

    });
  }
);
