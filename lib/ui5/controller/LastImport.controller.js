sap.ui.define(
  ["sap/ui/core/mvc/Controller", "niepce/controller/medialist/LastImport"],
  function (Controller, LastImportMediaList) {
    "use strict";
    return Controller.extend("niepce.controller.LastImport", {
      onInit: function () {
        this.oLastImport = new LastImportMediaList(this, "lastImport", "liGridList", "liPage", "GridCarousel");
      },
      onPatternMatched: function (oEvent) {
        this.oLastImport.onPatternMatched(oEvent);
      },
    });
  }
);

