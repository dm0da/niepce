sap.ui.define(
  ["sap/ui/core/mvc/Controller", "niepce/controller/medialist/Search"],
  function (Controller, SearchMediaList) {
    "use strict";
    return Controller.extend("niepce.controller.Search", {
      onInit: function () {
        this.oSearch = new SearchMediaList(this, "search", "searchGridList", "searchPage", "GridCarousel");
      },
      onPatternMatched: function (oEvent) {
        this.oSearch.onPatternMatched(oEvent);
      },
    });
  }
);
