
###########
# config5.ru
#

require 'thin'
require 'rack/csrf'

# simple redirect middleware for doing / => /ui5/index.html 

module Niepce
  class Pie
    def initialize(app, options={})
      @app = app
      @redir = options[:redir] || {}
      @erb = options[:erb] || []
    end
    def call(env)
      if @redir.has_key?(pi = env['PATH_INFO'])
        destination  = "#{env['rack.url_scheme']}://#{env['HTTP_HOST']}#{@redir[pi]}"
        destination << "?#{qs}" unless (qs=env['QUERY_STRING']).empty?
        [301, {'Location' => destination}, ['See Ya!']] 
# quick and dirty ERB templating        
      elsif @erb.include?(pa = env['PATH_INFO'])
        path = "#{pa}.erb".del_prefix('/')
        [200, {}, [ ERB.new(File.read(path)).result(NiepA.get_binding) ] ]
      else
        @app.call(env)
      end
    end
  end
end


niepce = Rack::Safrano::Builder.new  do

  use Rack::ODataCommonLogger if NiepA.log
  
  use Rack::Cors do
    allow do
      origins 'localhost:9494', '127.0.0.1:9494'
      resource '*',
        methods: [:get, :post, :delete, :put, :patch, :options, :head],
        headers: :any
    end
  end

  use Rack::Session::Pool 
  use Rack::Csrf, :raise => false, :skip => ['POST:/.*']
  
# Serve as  TwoWay+$batch ui5 app
  
  ui5d = NiepA.ui5d
  idxh = "/#{ui5d}/index.html"
  cmpjs = "/#{ui5d}/Component.js"
  
  redirects =  {'/'    => idxh, 
                '/ui5' => idxh,
                '/ui5/' => idxh}
  redirects["/#{ui5d}"] = idxh 
  redirects["/#{ui5d}/"] = idxh    

  # some user friendly redirects  
  # Note: We need this because serving / with /ui5/index.html content does not work
  # as is produces follow-up javascript resource requests like "/Component.js" 
  # outside of ui5 directory and it gives NOT FOUND
  # and also some minimal ERB templates handling
  
  use Niepce::Pie, redir: redirects, erb: [idxh, cmpjs]
  
  # serve local openui5 runtime from one directory up (avoiding symlinks)
  use Rack::Static,  :urls => ["/resources"], :root => "openui5-runtime-#{OpenUI5::VERSION}"
  
  # serve the ui5 app from ui5 
  use Rack::Static,  :urls => ["/#{ui5d}"]
  
  
# Serve the OData (prefixed /odata )
  run Niepce::ODataServer.new
  
end

Thin::Server.start('0.0.0.0', 9494, niepce)
