
# frozen_string_literal: true
require 'rack'
require 'sequel'
require 'fileutils'
require 'erb'
require_relative 'version'
require_relative 'utils'

require 'pry'  # for developement 

# uncomment this to use a local safrano lib
salib = '~/dev/Safrano/safrano/lib'
$LOAD_PATH.unshift(salib) unless $LOAD_PATH.include?(salib)

require 'safrano'
require 'exiftool_vendored'
require_relative './media_handler'

module Niepce
  class Server
    CONFIGDIRNAME = 'niepce'
    DBFILENAME = 'niepce.db3'
    CONFIGRU = 'config5.ru'
    MEDIADIR = 'media'
        
    attr_reader :db
    attr_reader :ui5d
    attr_reader :resources
    attr_reader :role
    attr_reader :host
    attr_reader :port
    attr_reader :media_root
    attr_accessor :log
    
    def initialize(options={})
      options ||= {}
      @role = options[:role]
# from libdir we readonly. --> can be a system (install) dir       
      @libdir = __dir__.dup
      
# apphome needs to be user-writeable !
      @apphome = if ( @role == :test )
        File.expand_path('testdb', @libdir)
      else
        gconfigdir = File.expand_path('.config', Dir.home)
        File.expand_path(CONFIGDIRNAME, gconfigdir)
      end
      
      $LOAD_PATH.unshift(@libdir) unless $LOAD_PATH.include?(@libdir)
      
      @systemdbname = File.join(@libdir, DBFILENAME)
      @homedbname = File.expand_path(DBFILENAME, @apphome)
      @media_root = File.expand_path(MEDIADIR, @apphome)
      @configru = File.expand_path(CONFIGRU, @libdir)

      @use_cdn = options[:use_cdn]
      
      # in testmode we dont log per default,
      # in non testmode we log per default,
      #  but this can be overriden with option
      @log = ( options[:log] || ( @role != :test ) )
      
      @resources = @use_cdn ? "https://openui5.hana.ondemand.com/#{OpenUI5::VERSION}/resources/sap-ui-core.js"  : '../resources/sap-ui-core.js'

      @ui5d = 'ui5'
      @port = 9494
      @host = if ( ( @role == :test ) && ENV['selenium_remote_url'] )
        Niepce.test_host_ip()
      else
#        'localhost'
        Niepce.serverhostname()
      end
    end
    
    def setup_db_in_homedir
      FileUtils.mkdir_p(@apphome) unless Dir.exist?(@apphome)
      if ( @role == :test )
        # in test-mode always start with a fresh DB copy
        FileUtils.cp(@systemdbname, @homedbname)
        
        # and reset the media directory 
        media_photo_dir = File.absolute_path('Media', @media_root)
        FileUtils.remove_entry_secure(media_photo_dir) if File.exists?(media_photo_dir)
      else
        FileUtils.cp(@systemdbname, @homedbname) unless File.exist?(@homedbname)
      end
    end
   
    def startdb
      setup_db_in_homedir
  
      @db = Sequel::Model.db = Sequel.sqlite(@homedbname)

      Kernel.at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
  
    end

    # needed for ERB
    def get_binding
      binding
    end
    
    def serve
  
      puts "Niepce version #{Niepce::VERSION} on #{@host} config #{@configru}"
      begin
  # this requires safrano ~> 0.3.5
        puts "Safrano version #{Safrano::VERSION}"
      rescue
      end
      
      Dir.chdir @libdir do
       
#       this is equivalent to  'rackup config.ru' but in same process  
        ARGV.clear
        ARGV << @configru
        Rack::Server.start 
      
      end
    
    end
  
  end
end



