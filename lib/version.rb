# frozen_string_literal: true

module Niepce
  VERSION = '0.0.1'
end

module OpenUI5
  VERSION = '1.84.13'
end